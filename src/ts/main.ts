

import { AppManager } from './core/appManager/AppManager';
import { Constants } from './misc/Constants';

const app = new AppManager(Constants.APP_WIDTH, Constants.APP_HEIGH, true);

// register app for PIXI dev tools
(window as any).__PIXI_APP__ = AppManager.app;
