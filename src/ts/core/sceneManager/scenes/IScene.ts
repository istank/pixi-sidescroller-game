import { Container } from "pixi.js";

export interface IScene extends Container{
    init(): void;
    destroy(): void;
    start(): void;
    stop(): void;
    update(delta: number): void;
}