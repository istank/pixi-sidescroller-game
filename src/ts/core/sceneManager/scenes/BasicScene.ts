import { Application, Container } from "pixi.js";
import { IScene } from "./IScene";

/**
 * Basic Scene template
 */
export class BasicScene extends Container implements IScene{

    public get active(): boolean{
        return this._active;
    }
    
    protected _active: boolean;
    private _initialized: boolean;

    public constructor() {
        super();
        this._active = false;
        this._initialized = false;
    }

    /**
    * called before the scene starts
    */
    public init(): void{
        this._initialized = true;
    }

    /**
     * called when the scene starts and becomes active in the scene Manager. 
     * The scene will now be rendered
     */
    public start(): void{
        if(!this._initialized){
            this.init();
        }

        this._active = true;
        this.visible = true;
    }

    /**
     * called when the scene is not active in the Scene Manager. 
     * The scene won't be rendered 
     */
    public stop(): void {
        this._active = false;
        this.visible = false;
    }

    /**
     * Called on every PIXI update tick while the scene is active
     * @param {number} deltaTime - elapsed time since the last update in milliseconds
     */
    public update(deltaTime: number): void{
        // do something
    }
}