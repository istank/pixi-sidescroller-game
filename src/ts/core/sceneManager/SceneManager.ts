import { AppManager } from "../appManager/AppManager";
import { IScene } from "./scenes/IScene";

/**
 * Manages scenes rendering and updates
 */
export class SceneManager {
    
    private _scenes: { [name: string]: IScene };
    private _activeScene: string|undefined;

    public constructor(){
        this._scenes = {};
        this._activeScene = undefined;
        AppManager.app.ticker.add(this.update.bind(this));
    }

    private update(deltaTime: number): void {
        if(!this._activeScene){
            return;
        }

        this._scenes[this._activeScene].update(deltaTime);
    }

    public add(sceneName: string, scene: IScene){
        if(this._scenes[sceneName]){
            throw new Error(`Already added scene with name ${sceneName}`);
        }
        scene.visible = false;
        this._scenes[sceneName] = scene;
        //add the scene as a child to the appmanager
        AppManager.app.stage.addChild(scene);
        
        if(!this._activeScene){
            this.setActiveScene(sceneName);
        }
    }

    public setActiveScene(sceneName: string) {
        if (!this._scenes[sceneName]) {
            throw new Error(`Scene with name ${sceneName} doesn't exist!`);
        }

        if(this._activeScene){
            this._scenes[this._activeScene].stop();
        }
        
        this._scenes[sceneName].start();
        this._activeScene = sceneName;
    }
}