import { Key } from "./Key";
/**
 * This will handle player keyboard input
 */
export class InputManager {

    private keyList: Key[] = [];

    private _axis: {[key: string] : number}= {horizontal: 0, vertical: 0}

    public get axis() {
        return this._axis;
    }

    public constructor(){
        window.addEventListener("keydown", this.onKeyDown.bind(this),false);
        window.addEventListener("keyup", this.onKeyUp.bind(this));
        
        this.addInputListeners();
    }

    private addInputListeners() {
        this.addKey("ArrowLeft", "horizontal", -1);
        this.addKey("ArrowRight", "horizontal", 1);
        this.addKey("ArrowUp", "vertical", -1);
        this.addKey("ArrowDown", "vertical", 1);
    }

    public addKey(keyID: string, axis: string, axisValue: number) {
        let key: Key = {
            keyID: keyID,
            axis: axis,
            axisValue: axisValue
        };

        this.keyList.push(key);

        return key;
    }

    private onKeyDown(event: KeyboardEvent) {
        for (let i: number = 0; i < this.keyList.length; i++) {
            let key: Key = this.keyList[i];

            if (event.key === key.keyID) {
                this._axis[key.axis] = key.axisValue;
                event.preventDefault();

                break;
            }
        }
    }

    private onKeyUp(event: KeyboardEvent) {
        for (let i: number = 0; i < this.keyList.length; i++) {
            let key: Key = this.keyList[i];

            if (event.key === key.keyID) {
                if(this._axis[key.axis]===key.axisValue){
                    this._axis[key.axis] = 0;
                }
                event.preventDefault();
                break;
            }
        }
    }
}