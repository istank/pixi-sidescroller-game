export interface Key {
    keyID: string;
    axis: string,
    axisValue: number
}