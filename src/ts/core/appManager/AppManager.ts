import { Application, Assets } from "pixi.js";
import { SceneManager } from "../sceneManager/SceneManager";
import { GameScene } from "../../game/scenes/GameScene";
import { MenuScene } from "../../game/scenes/MenuScene";
import { gsap } from "gsap";
import { FontStylesLoader } from "../FontStylesLoader";
import { SoundManager } from "../SoundManager";
import { GameOverScreen } from "../../game/scenes/GameOverScene";
/**
 * Manages the pixi application
 */
export class AppManager{
    private static instance: AppManager;
    private _config: any;
    private _app: Application;
    private _scenes!: SceneManager;
    private _debugEnabled: boolean;
    private static readonly gameMods = ["easy", "normal", "hard"];

    private _difficulty: 0 | 1 | 2 = 1;

    public static set difficulty(difficulty: 0 | 1 | 2){
        this.instance._difficulty = difficulty;
    }
    public static get START_LIVES(): number{
        return this.instance._config.playerLives[this.gameMods[this.instance._difficulty]];
    }

    public static get SCORE_MULTIPLIER() {
        return this.instance._config.difficultyScoreMultiplier[this.gameMods[this.instance._difficulty]];
    }

    public static get app(): Application {
        return this.instance._app;
    }
    public static get height(): number {
        return this.app.view.height;
    }
    public static get width(): number {
        return this.app.view.width;
    }
    public static get sceneManager(): SceneManager {
        return this.instance._scenes;
    }
    public static get debugEnabled(): boolean {
        return this.instance._debugEnabled;
    }

    public constructor(width: number, height: number, isDebugEnabled: boolean = false){
        AppManager.instance = this;
        
        this._debugEnabled = isDebugEnabled;
        this._app = new Application({
            width: width,
            height: height,
            backgroundColor: 0x315c90,
            view: document.getElementById('game-canvas') as HTMLCanvasElement,
        });
        this.setup();
    }
    async setup() {
        this._config = await Assets.load("./assets/config/gameConfig.json");
        this._debugEnabled = this._config.isDebug;
        await SoundManager.init();
        await FontStylesLoader.loadFonts();

        this._scenes = new SceneManager();
        this._scenes.add("GameScene", new GameScene());
        this._scenes.add("MenuScene", new MenuScene());
        this._scenes.add("GameOver", new GameOverScreen());
        this._scenes.setActiveScene("MenuScene");
    }
}
