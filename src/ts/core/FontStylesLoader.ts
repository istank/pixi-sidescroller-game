import { Assets } from "pixi.js";

export class FontStylesLoader{
    private static readonly CONFIG_PATH = "./assets/config/fonts.json"
    private static _fontsConfig: any;

    public static async loadFonts(): Promise<void> {
        this._fontsConfig = await Assets.load(this.CONFIG_PATH);
    }

    public static getFontStyle(fontName: string){
        return this._fontsConfig.fonts[fontName];
    }
}