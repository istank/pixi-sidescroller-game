import { Assets, Container, ITextStyle, Sprite, Text, TextStyle } from "pixi.js";
import { SoundManager } from "../SoundManager";

export class Button extends Container{
    private _assetsPath: string;
    private _default!: Sprite;
    private _hover!: Sprite;
    private _pressed!: Sprite;
    private _textField!: Text;
    public constructor(assets:string, text: string, style: Partial<ITextStyle>, onPressed: any){
        super();
        this._assetsPath = assets;

        this.init(text, style);
        this.eventMode = "dynamic";

        this.on("pointerenter", () => {
            this._hover.visible = true;
            this._default.visible = false;
            this._pressed.visible = false;
        });

        this.on("pointerleave", () => {
            this._hover.visible = false;
            this._default.visible = true;
            this._pressed.visible = false;
        });
        this.on("pointerdown", () => {
            this._hover.visible = false;
            this._default.visible = false;
            this._pressed.visible = true;

            SoundManager.playSound("click", false);

            onPressed();
        });
    }

    private async init(buttonText: string, style: Partial<ITextStyle>) {
        await this.loadAssets();

        this._textField = new Text(buttonText, new TextStyle(style));
        this._textField.anchor.set(0.5);
        this.addChild(this._textField);
    }

    public async loadAssets(): Promise<void> {
        return new Promise(async (resolve)=>{
            const sheet = await Assets.load(this._assetsPath);
            this._default = new Sprite(sheet.textures["default"]);
            this._default.anchor.set(0.5);
            this.addChild(this._default); 
            
            this._hover = new Sprite(sheet.textures["hover"]);
            this._hover.anchor.set(0.5);
            this.addChild(this._hover);
            this._hover.visible = false;

            this._pressed = new Sprite(sheet.textures["pressed"]);
            this._pressed.anchor.set(0.5);
            this._pressed.visible = false;
            this.addChild(this._pressed);
            resolve();
        });
    }
}