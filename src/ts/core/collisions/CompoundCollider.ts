import { Container } from "pixi.js";
import { BoxCollider } from "./BoxCollider";
import { IBoxColliderData } from "./IBoxColliderData";
import { ICollider } from "./ICollider";

export class CompoundCollider implements ICollider {
    private _boxColliders: ICollider[];
    private _parent!: Container;
    private _onCollision: any;

    public get parent(): Container {
        return this._parent;
    }

    public constructor(onCollision: any, ...collidersData: ICollider[]){
        this._onCollision = onCollision; 
        this._boxColliders = collidersData;
        if(this._boxColliders.length > 0){
            this._parent = this._boxColliders[0].parent;
        }
    }

    public onCollision(other: ICollider): void {
        if (this._onCollision) {
            this._onCollision(other);
        }
    }

    public checkColision(other: ICollider): boolean {
        this._boxColliders.forEach((collider)=>{
            if (collider.checkColision(other)) {
                other.onCollision(this);
                this.onCollision(other);
                return true;
            }
        });

        return false;
    }
}