import { Container } from "pixi.js";

export interface ICollider{
    checkColision(other: ICollider): boolean;
    onCollision(other: ICollider): void;
    get parent(): Container;
}