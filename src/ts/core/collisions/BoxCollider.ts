import { Container, Graphics, IPointData, Point, Rectangle } from "pixi.js";
import { IBoxColliderData } from "./IBoxColliderData";
import { ICollider } from "./ICollider";
import { CompoundCollider } from "./CompoundCollider";
import { Utils } from "../../misc/utils";
import { CircleCollider } from "./CircleCollider";
import { AppManager } from "../appManager/AppManager";

export class BoxCollider implements ICollider{
    private _data: IBoxColliderData;

    private _box!: Graphics;
    private _onCollision: any;
    public get box(): Graphics{
        return this._box;
    }
    private _width!: number
    public get width(): number {
        return this._width;
    }
    private _height!: number
    public get height(): number {
        return this._height;
    }
    public get parent(): Container{
        return this._data.parent;
    }
    public constructor(colliderData: IBoxColliderData, onCollision: any){
        this._data = colliderData;
        this._onCollision = onCollision;
        this.setupBox();
    }

    private setupBox(): void {
        const graphic = new Graphics();
        graphic.beginFill(0x00FF00, AppManager.debugEnabled ? 0.4 : 0);
        graphic.drawRect(0, 0, this._data.width, this._data.height);
        graphic.endFill();
        graphic.position = this._data.offset;
        this._data.parent.addChild(graphic);

        this._box = graphic;
        this._width = this._data.width;
        this._height = this._data.height;
    }
    
    public checkColision(other: ICollider): boolean {
        let isCollision = false;
        // if the other collider is compound collider, then let it check if some of its boxes collide with this one
        if(other instanceof CompoundCollider){
            isCollision = other.checkColision(this);
        }

        if(other instanceof BoxCollider){
            isCollision = Utils.checkCollisionBetweenRectangles(this, other);
        }

        if(other instanceof CircleCollider){
            isCollision = Utils.checkCollisionCircleAndRect(other, this);
        }
    
        if(isCollision){
            this.onCollision(other);
        }

        return isCollision;
    }

    public onCollision(other: ICollider): void {
        if(this._onCollision){
            this._onCollision(other);
        }
    }
}