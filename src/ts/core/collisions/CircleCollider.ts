import { Container, DisplayObject, Graphics, IPointData } from "pixi.js";
import { ICollider } from "./ICollider";
import { AppManager } from "../appManager/AppManager";
import { CompoundCollider } from "./CompoundCollider";
import { BoxCollider } from "./BoxCollider";
import { Utils } from "../../misc/utils";

export class CircleCollider implements ICollider{
    private _parent: Container;
    private _radius: number;
    private _offset: IPointData;
    private _circle!: Graphics;
    private _onCollision: any;

    public get parent(): Container<DisplayObject> {
        return this._parent;
    }
    public get circle(): Graphics{
        return this._circle;
    }
    private _width!: number;
    public get width(): number {
        return this._width;
    }
    private _height!: number;
    public get height(): number {
        return this._height;
    }
    public constructor(radius: number, offset: IPointData, parent: Container, onCollision: any){
        this._parent = parent; 
        this._radius = radius;
        this._offset = offset;

        this._onCollision = onCollision;
        this.setupGraphic();
    }

    private setupGraphic(): void {
        this._circle = new Graphics();
        this._circle.beginFill(0x00FF00, AppManager.debugEnabled ? 0.4 : 0);
        this._circle.drawCircle(0, 0, this._radius);
        this._circle.endFill();
        this._parent.addChild(this._circle);
        this._width = this._radius * 2;
    }

    public checkColision(other: ICollider): boolean {
        let isCollision = false;
        // if the other collider is compound collider, then let it check if some of its boxes collide with this one
        if (other instanceof CompoundCollider) {
            isCollision = other.checkColision(this);
        }

        if (other instanceof BoxCollider) {
            isCollision = Utils.checkCollisionCircleAndRect(this, other);
        }

        if (other instanceof CircleCollider) {
            isCollision = Utils.checkCircleCollision(this, other);
        }
        if(isCollision){
            this.onCollision(other);
        }
        return isCollision;
    }
    public onCollision(other: ICollider): void {
        if(this._onCollision){
            this._onCollision(other);
        }
    }
}