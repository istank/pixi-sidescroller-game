import { Container, IPointData } from "pixi.js";

export interface IBoxColliderData{
    width: number;
    height: number;
    parent: Container;
    offset: IPointData;
}