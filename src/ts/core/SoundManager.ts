import { Sound, SoundLibrary } from '@pixi/sound';
import { Assets } from 'pixi.js';

export class SoundManager {
    private static _soundLibrary: SoundLibrary;

    public static async init() {
        this._soundLibrary = new SoundLibrary();
        const config = await Assets.load("./assets/config/sounds.json");

        config.sounds.forEach((soundDef: any) => {
            this.addSound(soundDef.alias, soundDef.path);
        });
    }

    public static addSound(alias: string, path: string): void {
        // Load a sound and add it to the library
        this._soundLibrary.add(alias, path);
    }

    public static playSound(alias: string, loop: boolean): void {
        // Play a sound from the library
        this._soundLibrary.play(alias, {loop: loop});
    }

    public static stopSound(alias: string): void {
        // Stop a sound from the library
        this._soundLibrary.stop(alias);
    }
}
