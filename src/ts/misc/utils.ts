import { Circle, Container, DisplayObject, Graphics, IPointData, Point, Rectangle, Text } from "pixi.js";
import { IObstacle } from "../game/enemies/IObstacle";
import { FontStylesLoader } from "../core/FontStylesLoader";
import { CircleCollider } from "../core/collisions/CircleCollider";
import { BoxCollider } from "../core/collisions/BoxCollider";

export class Utils{
    // Helper function to calculate the distance between two points
    public static distance(x1: number, y1: number, x2: number, y2: number): number {
        const dx = x1 - x2;
        const dy = y1 - y2;
        return Math.sqrt(dx * dx + dy * dy);
    }

    // Helper function to clamp a value between two bounds
    public static clamp(value: number, min: number, max: number): number {
        return Math.max(min, Math.min(value, max));
    }

    // Method to check collision between a circle and a rectangle
    public static checkCollisionCircleAndRect(circle: CircleCollider, rect: BoxCollider): boolean {
        // Calculate the closest point on the rectangle to the circle
        let closestX = this.clamp(circle.circle.worldTransform.tx, rect.box.worldTransform.tx, rect.box.worldTransform.tx + rect.width);
        let closestY = this.clamp(circle.circle.worldTransform.ty, rect.box.worldTransform.ty, rect.box.worldTransform.ty + rect.height);

        // Calculate the distance between the circle's center and the closest point on the rectangle
        const distanceToClosestPoint = this.distance(circle.circle.worldTransform.tx, circle.circle.worldTransform.ty, closestX, closestY);

        // Check if the distance is less than the circle's radius (collision occurred)
        return distanceToClosestPoint < circle.width/2;
    }

    // Function to check collision between two circles
    public static checkCircleCollision(circle1: CircleCollider, circle2: CircleCollider): boolean {

        // Calculate the distance between the centers of the two circles
        const distanceBetweenCenters = this.distance(circle1.circle.worldTransform.tx, circle1.circle.worldTransform.ty, circle2.circle.worldTransform.tx, circle2.circle.worldTransform.ty);

        // Check if the distance is less than the sum of the two radiuses (collision occurred)
        return distanceBetweenCenters < circle1.width/2 + circle2.width/2;
    }

    public static checkCollisionBetweenRectangles(rect1: BoxCollider, rect2: BoxCollider): boolean{
        return rect1.box.worldTransform.tx                < rect2.box.worldTransform.tx + rect2.width
            && rect1.box.worldTransform.tx + rect1.width  > rect2.box.worldTransform.tx
            && rect1.box.worldTransform.ty                < rect2.box.worldTransform.ty + rect2.height
            && rect1.box.worldTransform.ty + rect1.height > rect2.box.worldTransform.ty;
    }

// check if object is of typ enemy
    public static checkIfObstacle(obj: any): obj is IObstacle  {
        
        return obj.obstacleName !== "";
    }

    public static setupText(value: string, textStyleName: string, position: IPointData, anchorX: number, parent: Container) {
        const textField = new Text(value, FontStylesLoader.getFontStyle(textStyleName));
        textField.anchor.x = anchorX;
        textField.anchor.y = 0.5;
        textField.pivot.set(0.5);
        textField.position = position;
        parent.addChild(textField);
        return textField;
    }
}
