import { PlayerController } from "../../player/PlayerController";
import { Missile } from "./Missile";

export class ChasingMissile extends Missile {
    public obstacleName: string = "ChasingMissile";

    private _chaseTime: number;
    private _currentChaseTime: number;
    public constructor(player: PlayerController, speed: number, chaseTime: number){
        super(player, speed);
        this._chaseTime = chaseTime;
        this._currentChaseTime = 0;
    }
    public update(deltaTime: number): void {
        if(this._currentChaseTime < this._chaseTime){
            this.calculateMovementVector();
            this._currentChaseTime += deltaTime;
        }
        super.update(deltaTime);
    }

    public spawnAtRandomPosition(): void {
        super.spawnAtRandomPosition();

        this._currentChaseTime = 0;
    }
}