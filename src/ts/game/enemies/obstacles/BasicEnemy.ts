import { Assets, Container, IPointData, Point, Sprite, Texture } from "pixi.js";
import { IObstacle } from "../IObstacle";
import { ICollider } from "../../../core/collisions/ICollider";
import { AppManager } from "../../../core/appManager/AppManager";

export class BasicEnemy extends Container implements IObstacle{

    public obstacleName: string = "BasicEnemy";
    protected _assetName: string = "";

    protected _collider!: ICollider;
    protected _sprite!: Sprite;

    protected _moveVector: IPointData;
    protected _speed: number;

    public get collider(): ICollider {
        return this._collider;
    }

    public constructor(moveVector: IPointData, speed: number){
        super();
        this._moveVector = moveVector;
        this._speed = speed;
        this.visible = false;
        this.position.set(-100); // set position out of screen to prevent double collision
    } 

    public update(deltaTime: number): void {
        if(!this._sprite || !this.visible){
            return;
        }

        this.position.x += this._moveVector.x * this._speed * deltaTime;
        this.position.y += this._moveVector.y * this._speed * deltaTime;

        if(this.position.x < -this.width || this.position.y > AppManager.height || this.position.y < -200){
            this.visible = false;
        }
    }

    public spawnAtRandomPosition(): void {
        const spawnX = AppManager.width * Math.max(Math.random(), 0.35);
        this.position = new Point(spawnX, 0);
        this.visible = true;
    }

    public onCollideWithPlayer(playerCollider: ICollider): void {
        this.visible = false;
        this.position.set(-100);
        this.updateTransform();
        console.log("Collided with player");
    }
    
    public async setupAsset() {
        const texture: Texture = await Assets.load(this._assetName);
        this._sprite = new Sprite(texture);
        this.addChild(this._sprite);
        (this._sprite as Sprite).anchor.set(0.5);
        this._sprite.pivot.set(0.5);
    }
}