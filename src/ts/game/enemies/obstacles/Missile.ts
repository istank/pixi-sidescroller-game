import { Container, DEG_TO_RAD, IPointData, PI_2, Point } from "pixi.js";
import { BasicEnemy } from "./BasicEnemy";
import { BoxCollider } from "../../../core/collisions/BoxCollider";

import { AppManager } from "../../../core/appManager/AppManager";
import { PlayerController } from "../../player/PlayerController";

export class Missile extends BasicEnemy {

    public obstacleName: string = "Missile";

    protected _assetName: string = "./assets/textures/missile.png";
    private _player: PlayerController;
    private _shooter: Container | undefined
    public constructor(player: PlayerController, speed: number, shooter?: Container){
        super(new Point(), speed);
        this._player = player;
        this._shooter = shooter;
        this.calculateMovementVector();
    }

    protected calculateMovementVector(): void {
        const angle = Math.atan2(this._player.y - this.y, this._player.x - this.x);
        const moveX = Math.cos(angle);
        const moveY = Math.sin(angle);
        const magnitude = Math.sqrt(moveX * moveX + moveY * moveY);
        this._moveVector = new Point(moveX/magnitude, moveY/magnitude);
        this.rotation = angle + PI_2/2;
    }

    public async setupAsset(): Promise<void> {
        await super.setupAsset();

        this._collider =  new BoxCollider({
                width: this._sprite.width ,
                height: this._sprite.height / 3,
                parent: this._sprite,
                offset: new Point(-this._sprite.width/2, -this._sprite.height/2 + 12)
        }, this.onCollideWithPlayer.bind(this));
    }

    public spawnAtRandomPosition(): void {
        this.position = this._shooter? this._shooter.position: new Point(AppManager.width, Math.random() * AppManager.height);
        
        this.calculateMovementVector();
        this.visible = true;
    }
}