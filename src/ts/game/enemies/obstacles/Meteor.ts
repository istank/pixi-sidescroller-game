import { DEG_TO_RAD, IPointData, Point, RAD_TO_DEG } from "pixi.js";
import { BasicEnemy } from "./BasicEnemy";
import { CircleCollider } from "../../../core/collisions/CircleCollider";

export class Meteor extends BasicEnemy {
    public obstacleName: string = "Meteor";

    protected _assetName: string = "./assets/textures/meteor.png";

    private _rotationSpeed: number = 5;

    public constructor(moveVector: IPointData, speed: number){
        super(moveVector, speed);
    }

    public async setupAsset(): Promise<void> {
        await super.setupAsset();

        this._collider = new CircleCollider(35, new Point(0), this, this.onCollideWithPlayer.bind(this));
    }

    public update(deltaTime: number): void {
        super.update(deltaTime);

        this.animate(deltaTime);
    }

    private animate(deltaTime: number): void {
        this.rotation += DEG_TO_RAD * this._rotationSpeed* deltaTime; 
    }
    
}