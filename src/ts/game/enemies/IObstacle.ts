import { Container, IPointData } from "pixi.js";
import { ICollider } from "../../core/collisions/ICollider";
import { EnemySpawner } from "./EnemySpawner";

export interface IObstacle extends Container {
    obstacleName: string;
    get collider(): ICollider;
    update(deltaTime: number): void;
    onCollideWithPlayer(playerCollider: ICollider): void;
    setupAsset(): void;
    spawnAtRandomPosition(): void;
}