export enum EnemyTypes{
    METEOR = 1,
    MISSILE = 2,
    CHASING_MISSILE = 3,
    SHOOTING = 4,
}