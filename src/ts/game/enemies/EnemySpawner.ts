import { Container, Point } from "pixi.js";
import { EnemyTypes } from "./EnemyTypes";
import { PlayerController } from "../player/PlayerController";
import { Meteor } from "./obstacles/Meteor";
import { IObstacle } from "./IObstacle";
import { ChasingMissile } from "./obstacles/ChasingMissile";
import { Missile } from "./obstacles/Missile";
import { ShootingEnemy } from "./shootingEnemy/ShootingEnemy";

type SpawnSettings = {
    enemyType: EnemyTypes;
    burst: number;
    spawnDelay: number;
    spawnCount: number;
    speedMin: number;
    // if speed max, it will spawn with random speed between the two
    startDelay: number;
    speedMax?: number;
    shooter?: Container;
    shooterMissile?: EnemyTypes.CHASING_MISSILE | EnemyTypes.MISSILE
}
export class EnemySpawner extends Container{
    private _player: PlayerController;
    private _settings: SpawnSettings;
    private _activeEnemies: IObstacle[] = [];
    private _inactiveEnemies: IObstacle[] = [];

    private _timeSinceLastSpawn: number;
    private _timeAlive: number;

    private _spawners: EnemySpawner[];

    public constructor(settings: SpawnSettings, player: PlayerController ){
        super();
        this._settings = settings;
        this._player = player;
        this._timeSinceLastSpawn = 0;
        this._timeAlive = 0;
        this._spawners = [];
        this.populateEnemies();
    }
    public addSpawner(spawner: EnemySpawner){
        this._spawners.push(spawner);
    }
    private populateEnemies(): void {
        for (let i = 0; i < this._settings.spawnCount; i++) {
            this.setupEnemy(this.getNewEnemy());
        }
    }

    private getNewEnemy() {
        const speed = this._settings.speedMax ? this.getRandomSpeed() : this._settings.speedMin;
        let enemy: IObstacle | undefined;
        switch (this._settings.enemyType) {
            case EnemyTypes.METEOR:
                enemy = new Meteor(new Point(-0.5, 1), speed);
                break;
            case EnemyTypes.CHASING_MISSILE:
                enemy = new ChasingMissile(this._player, speed, 90);
                break;
            case EnemyTypes.MISSILE:
                enemy = new Missile(this._player, speed, this._settings.shooter);
                break;
            case EnemyTypes.SHOOTING:
                enemy = new ShootingEnemy(this._player, new Point(-1,0),speed, this, this._settings.shooterMissile);
                break;
            default:
                break;
        }
        return enemy;
    }

    private setupEnemy(enemy: IObstacle | undefined) {
        (enemy as IObstacle).visible = false;
        (enemy as IObstacle).setupAsset();
        this.addChild((enemy as IObstacle));
        this._inactiveEnemies.push((enemy as IObstacle));
    }

    public update(deltaTime: number) {

        if(this._timeAlive < this._settings.startDelay){
            this._timeAlive += deltaTime;
            return;
        }
 
        this._timeSinceLastSpawn += deltaTime;
        const canSpawn = !this._settings.shooter || this._settings.shooter.visible;
        if (this._timeSinceLastSpawn > this._settings.spawnDelay && canSpawn) {
            this.spawnNewEnemies();
            this._timeSinceLastSpawn = 0;
        }

        this.updateEnemies(deltaTime);
    }

    private spawnNewEnemies(): void {
        if(this._settings.burst < 1){ this._settings.burst = 1;}

        for( let i = 0;  i < this._settings.burst; i++){
            const enemy = this._inactiveEnemies.pop();
            if(enemy){
                enemy.spawnAtRandomPosition();
                this._activeEnemies.push(enemy);
            } else {
                // add new enemy to the pool
                const newEnemy = this.getNewEnemy() as IObstacle;
                this.setupEnemy(newEnemy);
                this._activeEnemies.push(this._inactiveEnemies.pop() as IObstacle);
            }
        }
    }
    
    private updateEnemies(deltaTime: number) {
        this._activeEnemies.forEach((enemy, i) => {
            if (enemy.visible || enemy.collider) {
                enemy.update(deltaTime);
                
                enemy.collider.checkColision(this._player.collider);
            }
            if(!enemy.visible){
                this._inactiveEnemies.push(this._activeEnemies.splice(i,1)[0]);
            }
        });

        this._spawners.forEach((spawner)=>{
            spawner.update(deltaTime);
        });
    }

    checkBurst(currentBurst: number) {
        return this._settings.burst === 0 || currentBurst < this._settings.burst;
    }

    private getRandomSpeed() {
        return Math.random() * (this._settings.speedMax as number - this._settings.speedMin) + this._settings.speedMin;
    }
    public reset(): void {
        this._inactiveEnemies = this._inactiveEnemies.concat(this._activeEnemies);
        this._inactiveEnemies.forEach((enemy, i)=>{
            enemy.visible = false;
        });
        this._activeEnemies = [];
        this._timeSinceLastSpawn = 0;
        this._timeAlive = 0;

        this._spawners.forEach((spawner)=>{
            spawner.reset();
        })
    }
}