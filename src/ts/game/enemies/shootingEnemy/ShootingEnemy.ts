import { Container, IPointData, PI_2, Point } from "pixi.js";
import { CircleCollider } from "../../../core/collisions/CircleCollider";
import { BasicEnemy } from "../obstacles/BasicEnemy";
import { PlayerController } from "../../player/PlayerController";
import { AppManager } from "../../../core/appManager/AppManager";
import { EnemySpawner } from "../EnemySpawner";
import { EnemyTypes } from "../EnemyTypes";

export class ShootingEnemy extends BasicEnemy{
    public obstacleName: string = "ShootingEnemy";
    protected _assetName: string = "./assets/textures/shootingEnemy.png";

    private _gun: EnemySpawner;
    private _player: Container;
 
    public constructor(player: PlayerController, moveVector: IPointData, speed: number, gunParent: EnemySpawner, missileType?: EnemyTypes.CHASING_MISSILE | EnemyTypes.MISSILE) {
        super(moveVector, speed);
        this._gun = new EnemySpawner({
            enemyType: missileType ||EnemyTypes.MISSILE,
            burst: 1,
            spawnCount: 2,
            spawnDelay: 120,
            speedMin: 7,
            startDelay: 0,
            shooter: this,
        },
        player
        );
        gunParent.addChild(this._gun);
        gunParent.addSpawner(this._gun);
        this._player = player;
    }

    public async setupAsset(): Promise<void> {
        await super.setupAsset();

        this._collider = new CircleCollider(35, new Point(0), this, this.onCollideWithPlayer.bind(this));
    }

    public update(deltaTime: number): void {
        if (this.visible) {
            super.update(deltaTime);

            this.lookAtPlayer();
        }
    }

    private lookAtPlayer() {
        const angle = Math.atan2(this._player.y - this.y, this._player.x - this.x);
        this.rotation = angle + PI_2/4;
    }

    public spawnAtRandomPosition(): void {
        const spawnX = AppManager.width + this._sprite.width;
        this.position = new Point(spawnX, AppManager.height - this._sprite.height);
        this.visible = true;
    }

}