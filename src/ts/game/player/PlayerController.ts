import { Assets, Container, Point, Sprite, Texture } from "pixi.js";
import { InputManager } from "../../core/inputManager/InputManager";
import { AppManager } from "../../core/appManager/AppManager";
import { ICollider } from "../../core/collisions/ICollider";
import { CompoundCollider } from "../../core/collisions/CompoundCollider";
import { BoxCollider } from "../../core/collisions/BoxCollider";
import { Utils } from "../../misc/utils";
import { gsap } from "gsap";

export class PlayerController extends Container {
    private readonly ASSET_NAME= "./assets/textures/player_image.png";
    
    private _lowerBounds: Point = new Point();
    private _upperBounds: Point = new Point();

    private _input: InputManager;

    private _speed: number = 6;
    private _animationSpeed: number = 0.1;

    private _sprite!: Sprite;

    private _collider!: ICollider;

    private _timeSinceLastHit: number = 0;
    private _immuneTime: number = 30;

    private _onHitEnemy: any;
    private _onDead: any;
    private _lives: number = 3;

    public get collider(): ICollider {
        return this._collider
    }

    public get lives(): number {
        return this._lives;
    }

    public constructor(onHitEnemy: any, onDead: any){
        super();
        this._onDead = onDead;
        this._onHitEnemy = onHitEnemy;
        this.name = "Player";
        this._input = new InputManager();
        this.setupSprite();
    }

    public update(deltaTime: number) {
        if(this._sprite === undefined){
            return;
        }
        this.position.x += deltaTime * this._input.axis.horizontal * this._speed;
        this.position.y += deltaTime * this._input.axis.vertical * this._speed;

        this.checkBounds();

        this.animate(deltaTime);
        this._timeSinceLastHit += deltaTime;
    }


    private setupCollider(): void {
        this._collider = new CompoundCollider(
            this.onCollision.bind(this),
            new BoxCollider({
                width: this._sprite.width/2, 
                height: this._sprite.height, 
                parent: this, 
                offset: new Point(-this._sprite.width / 2, -this._sprite.height / 2)
            }, 
            undefined),
            new BoxCollider({
                width: this._sprite.width / 2,
                height: this._sprite.height / 4,
                parent: this,
                offset: new Point(0, -this._sprite.height / 8)
            },
            undefined
            )
        );
    }

    private onCollision(other: ICollider): void {
        if(Utils.checkIfObstacle(other.parent) && this._timeSinceLastHit > this._immuneTime){
            this._timeSinceLastHit = 0;

            this._lives = this._lives > 0? this._lives-1: 0;
            this.hitAnim();
            this._onHitEnemy(this._lives);
            if(this._lives <= 0){
                this._onDead();
            }
        }
    }

    private hitAnim(): void {
        gsap.timeline({repeat:1})
        .to(this._sprite, {alpha: 0, duration: 0.25})
        .to(this._sprite, {alpha: 1, duration: 0.25});
    }

    private animate(deltaTime: number): void {
        // if moving vertically
        let targetScaleY = this._input.axis.vertical !== 0 ? 0.65 : 1;
        // if moving forward decrease the Y scale more to give sense of more speed
        if (this._input.axis.horizontal > 0){ 
            targetScaleY = 0.6;
        }
        const targetScaleX = this._input.axis.horizontal > 0 ? 1.2 : 1;
        const scaleDeltaX = (targetScaleX - this.scale.x) * deltaTime * this._animationSpeed;
        const scaleDeltaY = (targetScaleY - this.scale.y) * deltaTime * this._animationSpeed;

        this.scale.y = this.scale.y + scaleDeltaY;
        this.scale.x = this.scale.x + scaleDeltaX;
    }

    private checkBounds() {
        // check for lower bound
        this.position.x = this.position.x < this._lowerBounds.x ? this._lowerBounds.x : this.position.x;
        this.position.y = this.position.y < this._lowerBounds.y ? this._lowerBounds.y : this.position.y;
        //check for upper bound
        this.position.x = this.position.x > this._upperBounds.x ? this._upperBounds.x : this.position.x;
        this.position.y = this.position.y > this._upperBounds.y ? this._upperBounds.y : this.position.y;
    }

    private async setupSprite(): Promise<void> {
        const texture: Texture = await Assets.load(this.ASSET_NAME);
        this._sprite = new Sprite(texture);
        this.addChild(this._sprite);
        this._sprite.anchor.set(0.5);

        this.setupBounds();

        this.setupCollider();
    }

    private setupBounds() {
        this._upperBounds.x = AppManager.app.screen.width - this._sprite.width / 2;
        this._upperBounds.y = AppManager.app.screen.height - this._sprite.height / 2;

        this._lowerBounds.x = this._sprite.width / 2;
        this._lowerBounds.y = this._sprite.height / 2;
    }
    
    public reset(): void {
        this.x = AppManager.width * 0.2;
        this.y = AppManager.height * 0.5;
        this._lives = AppManager.START_LIVES;
        this._timeSinceLastHit = 0;
    }
}