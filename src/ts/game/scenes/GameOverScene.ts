import { Point, Text } from "pixi.js";
import { BasicScene } from "../../core/sceneManager/scenes/BasicScene";
import { AppManager } from "../../core/appManager/AppManager";
import { Utils } from "../../misc/utils";
import { FontStylesLoader } from "../../core/FontStylesLoader";
import { Button } from "../../core/button/Button";

export class GameOverScreen extends BasicScene{

    private readonly BUTTON_ASSET = "./assets/textures/buttons.json";
    private _button!: Button;
    private _highScoreValue: Text;
    private _highScoreText: Text;
    private _lastScoreValue: Text;
    public constructor() {
        super();

        Utils.setupText(
            "GAME OVER!", 
            "gameOverText", 
            new Point(AppManager.width / 2, AppManager.height *0.25),
            0.5,
            this
        );

        this._highScoreText =  Utils.setupText(
            "BEST:",
            "highScoreText",
            new Point(AppManager.width * 0.5 - 10, AppManager.height * 0.5),
            1,
            this
        );

        Utils.setupText(
            "LAST:",
            "lastScoreText",
            new Point(AppManager.width * 0.5 - 10, AppManager.height * 0.6),
            1,
            this
        );

        this._highScoreValue = Utils.setupText(
            "",
            "highScoreText",
            new Point(AppManager.width * 0.5 + 5, AppManager.height * 0.5),
            0,
            this
        );

        this._lastScoreValue = Utils.setupText(
            "",
            "lastScoreText",
            new Point(AppManager.width * 0.5 + 5, AppManager.height * 0.6),
            0,
            this
        );

        this.setupButton();
    }

    private setupButton() {
        this._button = new Button(this.BUTTON_ASSET, "RESTART", FontStylesLoader.getFontStyle("playButtonText"), () => {
            AppManager.sceneManager.setActiveScene("GameScene");
        });
        this.addChild(this._button);

        this._button.pivot.set(0.5);

        this._button.position.set(AppManager.width / 2, AppManager.height * 0.75);
    }

    public start(): void {
        super.start();

        this._highScoreValue.text = localStorage.getItem("bestScore") || "N/A";
        this._lastScoreValue.text = localStorage.getItem("lastScore") || "N/A";

        const newBest = localStorage.getItem("newBest")==="1";
        this._highScoreText.text = newBest? "NEW BEST!!!": "BEST:";
    }
}