import { Point } from "pixi.js";
import { BasicScene } from "../../core/sceneManager/scenes/BasicScene";
import { ScrollingBackground } from "../components/ScrollingBackground";
import { IObstacle } from "../enemies/IObstacle";
import { Meteor } from "../enemies/obstacles/Meteor";
import { PlayerController } from "../player/PlayerController";
import { AppManager } from "../../core/appManager/AppManager";
import { Missile } from "../enemies/obstacles/Missile";
import { ChasingMissile } from "../enemies/obstacles/ChasingMissile";
import { EnemySpawner } from "../enemies/EnemySpawner";
import { EnemyTypes } from "../enemies/EnemyTypes";
import { SoundManager } from "../../core/SoundManager";
import { UIController } from "../components/UIController";
import { gsap } from "gsap";

/**
 * This scene will contorl the game play 
 */
export class GameScene extends BasicScene{
    private _background: ScrollingBackground;

    private _player: PlayerController;
    private _enemySpawners: EnemySpawner[];

    private _ui: UIController;
    private _gameOver: boolean = false;

    private _scoreMultiplier: number = 0.01;
    private _score: number = 0;
    public constructor(){
        super();
        this._ui = new UIController();
        this._player = new PlayerController(this.onPlayerLivesChange.bind(this), this.onPlayerDead.bind(this));
        this._background = new ScrollingBackground();

        this.addChild(this._background);
        this.addChild(this._player);
        this.addChild(this._ui);

        this._enemySpawners = [];
        this.generateSpawners();
    }

    private onPlayerLivesChange(lives: number): void {
        this._ui.updateLives(lives);
    }

    private onPlayerDead(): void {
        this._gameOver = true;
        this.saveScore();

        gsap.delayedCall(1, ()=>{
            AppManager.sceneManager.setActiveScene("GameOver");
        });
    }
    private saveScore(): void {
        localStorage.setItem("lastScore", Math.round(this._score).toString());

        const bestScore = localStorage.getItem("bestScore") || "0";

        const newBestScore = parseInt(bestScore, 10) < this._score;

        localStorage.setItem("bestScore", newBestScore ? Math.round(this._score).toString() : bestScore);

        localStorage.setItem("newBest", newBestScore? "1": "0");
    }
    
    private generateSpawners(): void {
        const meteorSpawner = new EnemySpawner({
                enemyType: EnemyTypes.METEOR,
                burst: 2,
                spawnCount: 5,
                spawnDelay: 150,
                speedMin: 5,
                speedMax: 8,
                startDelay: 120,
            }, 
            this._player
        );
        this.addChild(meteorSpawner);
        this.setChildIndex(meteorSpawner, this.children.length - 2);
        this._enemySpawners.push(meteorSpawner);

        const missileSpawner = new EnemySpawner({
            enemyType: EnemyTypes.MISSILE,
            burst: 2, 
            spawnCount: 5,
            spawnDelay: 300,
            speedMin: 7,
            startDelay: 900,
        }, 
        this._player);
        this.addChild(missileSpawner);
        this.setChildIndex(missileSpawner, this.children.length - 2);
        this._enemySpawners.push(missileSpawner);


        const shootingEnemySpawner = new EnemySpawner({
            enemyType: EnemyTypes.SHOOTING,
            burst: 1,
            spawnCount: 2,
            spawnDelay: 300,
            speedMin: 5,
            startDelay: 1560,
        },
        this._player);
        this.addChild(shootingEnemySpawner);
        this.setChildIndex(shootingEnemySpawner, this.children.length - 2);
        this._enemySpawners.push(shootingEnemySpawner);


        const aimingMissileShooter = new EnemySpawner({
            enemyType: EnemyTypes.SHOOTING,
            burst: 1,
            spawnCount: 5,
            spawnDelay: 300,
            speedMin: 7,
            startDelay: 3600,
            shooterMissile: EnemyTypes.CHASING_MISSILE
        },
            this._player);
        this.addChild(aimingMissileShooter);
        this.setChildIndex(aimingMissileShooter, this.children.length - 2);
        this._enemySpawners.push(aimingMissileShooter);

        const aimingMissileSpawner = new EnemySpawner({
            enemyType: EnemyTypes.CHASING_MISSILE,
            burst: 1,
            spawnCount: 5,
            spawnDelay: 240,
            speedMin: 7,
            startDelay: 2100,
        },
            this._player);
        this.addChild(aimingMissileSpawner);
        this.setChildIndex(aimingMissileSpawner, this.children.length - 2);
        this._enemySpawners.push(aimingMissileSpawner);
    }

    public start(){
        super.start();

        this._ui.visible = true;
        this._score = 0;
        this._player.reset();
        this._ui.updateLives(this._player.lives);
        this._ui.updateScore(this._score);
        this._gameOver = false;
        this._scoreMultiplier = AppManager.SCORE_MULTIPLIER.base;
        this._enemySpawners.forEach((spawner)=>{
            spawner.reset();
        });
        this.alpha = 1;
        SoundManager.playSound("gameSound", true);
    }

    public stop(): void {
        this._ui.visible = false;
        SoundManager.stopSound("gameSound");
    }

    public update(deltaTime: number): void {
        if (!this._gameOver) {
            this._player.update(deltaTime);

            this._score += this._scoreMultiplier * deltaTime;
            this._scoreMultiplier += AppManager.SCORE_MULTIPLIER.increase * deltaTime;

            this._ui.updateScore(Math.floor(this._score));
        }
        this._background.update(deltaTime);
        this._enemySpawners.forEach((spawner)=>{
            spawner.update(deltaTime);
        })
    }
}