import { Assets, Sprite, Text, TextStyle } from "pixi.js";
import { BasicScene } from "../../core/sceneManager/scenes/BasicScene";
import { gsap } from "gsap";
import { Button } from "../../core/button/Button";
import { AppManager } from "../../core/appManager/AppManager";
import { Constants } from "../../misc/Constants";
import { FontStylesLoader } from "../../core/FontStylesLoader";
import { SoundManager } from "../../core/SoundManager";

export class MenuScene extends BasicScene{
    private readonly ASSETS =  {
        background: "./assets/textures/desert.png",
        button: "./assets/textures/buttons.json"
    };
    private _playNormal!: Button;
    private _playHard!: Button;
    private _playEasy!: Button;
    public constructor() {
        super();
        this.addBackground();
        this.setupButtons();
    }

    public init(): void {
        this.setupTitleTexts();
    }

    private async setupTitleTexts() {
        const title = new Text("SUPER", FontStylesLoader.getFontStyle("titleText"));
        title.anchor.set(0.5);
        title.pivot.set(0.5);
        title.skew.x = -0.25;
        title.skew.y = -0.1;
        title.position.set(AppManager.width * 0.36, AppManager.height*0.27);

        const subtitle = new Text("SPACESHIP", FontStylesLoader.getFontStyle("subtitleText"));
        subtitle.anchor.set(0.5);
        
        subtitle.position.set(AppManager.width / 2, AppManager.height * 0.4);

        this.addChild(subtitle);
        this.addChild(title);
    }

    private setupButtons(): void {
        this._playNormal = new Button(this.ASSETS.button, "NORMAL", FontStylesLoader.getFontStyle("playButtonText"), () => {
            AppManager.difficulty = 1;
            AppManager.sceneManager.setActiveScene("GameScene");
        });
        this.setupButton(this._playNormal, AppManager.height * 0.75);

        this._playEasy = new Button(this.ASSETS.button, "EASY", FontStylesLoader.getFontStyle("playButtonText"), () => {
            AppManager.difficulty = 0;
            AppManager.sceneManager.setActiveScene("GameScene");
        });
        this.setupButton(this._playEasy, AppManager.height * 0.65);

        this._playHard = new Button(this.ASSETS.button, "HARD", FontStylesLoader.getFontStyle("playButtonText"), () => {
            AppManager.difficulty = 2;
            AppManager.sceneManager.setActiveScene("GameScene");
        });
        this.setupButton(this._playHard, AppManager.height * 0.85);
    }

    private setupButton(button: Button, posY: number) {
        this.addChild(button);

        button.pivot.set(0.5);

        button.position.set(AppManager.width / 2, posY);
    }

    private async addBackground(): Promise<void> {
        const bgTexture = await Assets.load(this.ASSETS.background);
        const background = new Sprite(bgTexture);
        this.addChildAt(background, 0);
        background.tint = 0x3666A3;
    }

    public update(deltaTime: number): void {
        return;
    }

    public start(): void{
        super.start();
        SoundManager.playSound("mainMenuSound", true);
    }
    /**
     * called when the scene is not active in the Scene Manager. 
     * The scene won't be rendered 
     */
    public stop(): void {
        gsap.to(this, {
            alpha: 0, duration: 1, onComplete: () => {
                this._active = false;
                this.visible = false;
            }
        });
        SoundManager.stopSound("mainMenuSound");
    }
}