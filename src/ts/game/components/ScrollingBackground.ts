import { Assets, Container, Sprite, Texture } from "pixi.js";

export class ScrollingBackground extends Container{
    private readonly ASSET_NAME: string = "./assets/textures/desert.png";

    private _sprites: Sprite[] =[];

    private _speed: number;
    public constructor(speed: number = 5){
        super();
        this._speed = speed;
        this.setupSprites();
    }

    // scroll the backgrounds by moving the sprites on the X axis
    // Once one of the sprites is hidden from the view move it to the back.
    public update(deltaTime: number): void {
        if(this._sprites.length === 0){
            return;
        }
        this._sprites[0].x -= deltaTime * this._speed;
        this._sprites[1].x = this._sprites[1].x = this._sprites[0].x + this._sprites[0].width;

        // Switch sprites indexes if first sprite is off screen
        if(this._sprites[0].x< -this._sprites[0].width){
            const sprite = this._sprites.shift() as Sprite;
            this._sprites.push(sprite);
        }
    }

    // We will need two instances of the sprite to make it seamless scrolling
    private async setupSprites(): Promise<void> {
        const texture: Texture = await Assets.load(this.ASSET_NAME).then();
        const sprite = new Sprite(texture);
        const sprite2 = new Sprite(texture);
        this.addChild(sprite);
        this.addChild(sprite2);
        sprite2.x = sprite2.width;

        this._sprites.push(sprite);
        this._sprites.push(sprite2);
        sprite.tint = 0x6686A3;
        sprite2.tint = 0x6686A3;;
    
    }
}
