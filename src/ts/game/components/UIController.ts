import { Container, IPointData, Point, Text } from "pixi.js";
import { FontStylesLoader } from "../../core/FontStylesLoader";
import { AppManager } from "../../core/appManager/AppManager";
import { Utils } from "../../misc/utils";

export class UIController extends Container{
    private _livesTitle!: Text;
    private _livesValue!: Text;

    private _scoreTitle!: Text;
    private _scoreValue!: Text;
    public constructor(){
        super();

        this._livesTitle = Utils.setupText(
            "Lives:", 
            "livesTitleText", 
            new Point(AppManager.width * 0.1, AppManager.height * 0.075),
            1,
            this
        );
        this._livesValue = Utils.setupText(
            "", 
            "livesValueText", 
            new Point(this._livesTitle.x + 10, this._livesTitle.y),
            0,
            this
        );
        this._scoreTitle = Utils.setupText(
            "Score:", 
            "scoreTitleText",
            new Point(AppManager.width * 0.9, AppManager.height * 0.075),
            1,
            this
        );
        this._scoreValue = Utils.setupText(
            "0", 
            "scoreValueText", 
            new Point(this._scoreTitle.x + 10, this._scoreTitle.y),
            0,
            this
        );
    }

    public updateLives(lives: number): void {
        this._livesValue.text = lives;
    }

    public updateScore(score: number): void {
        this._scoreValue.text = score;
    }
}