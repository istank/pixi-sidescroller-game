# pixi-sidescroller-game



## Introduction
A basic sidescroller game where the player controls a space ship and avoids enemies and obstacles

Use arrow keys to move and avoid enemies. 

The more you stay alive, the faster your score will accumulate

## Getting Started

### Building the project
1. Run `npm install` to install the project dependencies

2. Run `npm run build_dev` to build the project

### Launching the game
To launch the game run

`npm run launch_dev`

load in your browser 

`http://localhost:8743/`

